{{/*
Expand the name of the chart.
*/}}
{{- define "application.name" -}}
{{- printf "%s" .Release.Name | trunc 63 | trimSuffix "-" }}
{{- end }}


{{/*
Return pullpolicy secret name.
*/}}
{{- define "application.pullsecret" -}}
{{- include "application.name" . }}-pullsecret
{{- end }}