require('dotenv').config();
const express = require('express');
const Knex = require('knex');
const server = express();

console.log(process.env.PG_CONNECTION);
const knex = Knex({
  client: 'pg',
  connection: process.env.PG_CONNECTION,
});

server.get('/', express.static('public'));

server.get('/clients', async (req, res) => {
  try {
    const clients = await knex.raw(`
        select name from public.clients where name like '%${req.query.client}%'`);
    res.send(clients.rows);
  } catch (err) {
    console.log(err);
    return res.status(500).send(err);
  }
});
const port = process.env.PORT || 3000;

server.listen(port, () => console.log(`running server on ${port}`));
