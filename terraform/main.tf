terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.20"
    }
  }
}
provider "aws" {
  profile = "default"
  region  = "eu-central-1"
}

resource "aws_key_pair" "k3s_key" {
  key_name   = "k3s_key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDBHAYqqDXQrSOL3KuI7VAI6I+nSs0l1isrPAI1ObJQc1kYOwdonRSlsCE28RMUpEWC72csDv4PARHL4DDglOjKokPlmtansL581WdHnwdtxtBCJmIpOwTptLdPGwPRTj0PlYuHFLfXFVWjw1vQXPQh6zH8sPVpNnXhWmBoJXRw8ueYza7SpXKaTq2fGIRoL+aHQBJMU9zbZWkDfNVbW50Y5TsJ20WkhPR2im+4ktquSRa1garyrg1WmmwAZY+M45Fku9WETv1AGtcoQogC6xWStTYgZEbdZw1f08u2Onxt6yNAiGWnkf5sTU/OV0d1aKu/9f4scxPHKhtJmt+H0cRBMATELmo9Pb8nDUJD37YFyWC10CiMMWe5PN7Mt23ByMr2eIyHNKwNGfF6kivp1K3kF5XHVA6bYLEpmICHJm+kPN7fbvI+MvqNbQ9o4aHRAPS7e/0FmJc8mWAGejwti02+nsTspVmMd/T/XkmpblA68gM7ofQHiAldY897GVlpWYk= sergeyshevch@SergeyShevchPC"
}

resource "aws_security_group" "k3s" {
  name        = "k3s-security-group"
  description = "Allow HTTP, HTTPS and SSH traffic"

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "k3s"
    from_port   = 6443
    to_port     = 6443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "terraform"
  }
}


resource "aws_instance" "k3s" {
  ami           = "ami-0bd39c806c2335b95"
  instance_type = "t2.micro"
  key_name      = aws_key_pair.k3s_key.key_name

  tags = {
    Name = "k3s"
  }

  vpc_security_group_ids = [
    aws_security_group.k3s.id
  ]

  user_data = <<EOT
    #! /bin/bash
    curl -sfL https://get.k3s.io | sh -
  EOT

  # mkdir /etc/rancher/k3s
  # IP=$(curl https://ipecho.net/plain)
  # echo '
  # tls-san:
  #   - "'$IP'"
  # ' > /etc/rancher/k3s/config.yaml
}
